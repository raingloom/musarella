runs as a 9P service

musarella/
	searchers/
		spotify/
			query
			response
		local/
			query
			response
	playing


example queries:

(musarella
	(track
		(title "Very Unique Title"))
	(track
		(title "Some Title")
		(performer (or "Real Band" "Also Good Cover Band")))
	(track
		(title (or "Official Title" "Popular But Not Official Title"))
		(performer (or (and "Main Band" "Accompanying Singer") "Original Band"))))


GET (simple-playlist "Foo Bar" "Asd Baz")
https://path/to/song
file://path/to/song
