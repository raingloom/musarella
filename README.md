# Musarella - Portable Playlist Platform
Very WIP. It'll be like XSPF but will actually have resolvers, not just be a
format for storing playlist metadata.

It's meant to replace playlists dependent on online services.

## Planned usage
```
$ echo 'query string goes here' | songurl
file://local/file/matching/your/query
http://example.com/streaming/music/same-song-but-from-an-online-source.mp3
```

`songurl` does no parsing, it is a simple [rc](http://man.cat-v.org/plan_9/1/rc)
script that launches resolvers

# Writing a resolver
Resolvers are executables that are expected to read a query string from stdin
and on success produce a valid result string
_(currently this is a URL but the spec is not final)_
on stdout. They must not produce any other output on stdout, debugging info, as
always, must go on stderr.

# Query protocol
The current idea is to use s-expressions because they are easy to parse.
Whether there is a need to make the protocol versioned is unclear.